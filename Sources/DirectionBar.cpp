#include "DirectionBar.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>


GLfloat verticesDirectionBar[] = {
	//Carr� du milieu
	-1.0f, 1.0f, 0.0f,
	-1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f,

	-1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,
	1.0f, 0.0f, 0.0f,

	-5.0f, 1.0f, 0.0f, //Pas modifier
	-5.0f, 0.0f, 0.0f, //Pas modifier
	5.0f, 0.0f, 0.0f,

	-5.0f, 1.0f, 0.0f, //Pas modifier
	5.0f, 1.0f, 0.0f,
	5.0f, 0.0f, 0.0f
};

GLfloat colorsDirectionBar[] = {
	255.0f, 0.0f, 0.0f,
	255.0f, 0.0f, 0.0f,
	255.0f, 0.0f, 0.0f,

	255.0f, 0.0f, 0.0f,
	255.0f, 0.0f, 0.0f,
	255.0f, 0.0f, 0.0f,

	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,

	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f
};

DirectionBar::DirectionBar()
{
	for (size_t i = 0; i < 36; i++)
		initVertices[i] = verticesDirectionBar[i];
	direction = 0;
}


DirectionBar::~DirectionBar()
{
}

void DirectionBar::show(Shader *s, glm::mat4 model, glm::mat4 camera, glm::mat4 projection) {
	construct();
	s->use();
	s->setMatrix4("M", model);
	s->setMatrix4("C", camera);
	s->setMatrix4("Pr", projection);
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void DirectionBar::construct() {
	GLuint VBO, VBOColor, VBOTexture;;

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesDirectionBar), verticesDirectionBar, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &VBOColor); // second VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBOColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorsDirectionBar), colorsDirectionBar, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0); // Unbind the VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind the VBO
}

int DirectionBar::getDirection()
{
	return direction;
}

void DirectionBar::resetDirection() {
	direction = 0;
	setAmount = 1;
	for (size_t i = 0; i < 36; i++)
		verticesDirectionBar[i] =  initVertices[i];
}

void DirectionBar::updateDirection()
{
	if (direction == 4)
		setAmount = -1;
	else if(direction == -4)
		setAmount = 1;

	direction += setAmount;
	for (size_t i = 0; i <= 15; i += 3)
		verticesDirectionBar[i] += setAmount;
}
