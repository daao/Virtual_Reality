// Local Headers
#include "glitter.hpp"

// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/string_cast.hpp"

#include "Shader.hpp"
#include "ModelObject/Cube.hpp"
#include "Camera.hpp"
#include "Model.hpp"
#include "ModelObject/PinModel.hpp"
#include "ModelObject/BallModel.hpp"
#include "ModelObject/RoomModel.hpp"
#include "ModelObject/TrackModel.hpp"
#include "ModelObject/EndTrackModel.hpp"
#include "ModelObject/LampModel.hpp"
#include "Util.hpp"
#include "PowerBar.h"
#include "DirectionBar.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>

#include "bullet/btBulletCollisionCommon.h"
#include "bullet/btBulletDynamicsCommon.h"

using namespace glm;

static bool keys[1024]; // is a key pressed or not ?
						// External static callback
						// Is called whenever a key is pressed/released via GLFW


// Tools for the game
PowerBar *powerBar;
DirectionBar* directionBar;
bool showPowerBar = true;
bool setPowerBar = false;
bool ball_transparency = false;
int directionBarDifficulty = 50;
bool startMovingBall = false;
bool giveImpulsion = false;
btDynamicsWorld* m_pWorld;

// Models
BallModel *Ball;
PinModel *Pin;
bool PinCollision[10];
RoomModel *Room;
TrackModel *Track;
EndTrackModel *EndTrack;
LampModel *Lamp;
Cube* map;
Camera* cam;

// Shaders
Shader *s;
Shader *texture;
Shader *lightning;
Shader *light_transp;
Shader *mapTexture;
Shader *shadow;


// Functions
void moveBall();
void restartGame();
void initBullet(int nbPins);
void initPins(int nbPins, Shader* s, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthCubemap);
void updatePins(int nbPins, Shader* s, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthCubemap);
void cleanWord();
void renderDepthMap(Shader *s, unsigned int depthMapFBO, float near_plane, float far_plane);
void renderSkybox(GLuint ourMapTexture);
void checkCollision();

// Textures & Window
GLFWwindow* createWindow();
GLuint static createTexture(std::string filePath);
GLuint cubeMapTexture();

// Matrix
glm::mat4 ModelBall;
glm::mat4 ProjectionMatrix;


// Callbacks
static void key_callback(GLFWwindow* window, int key, int, int action, int);
static void mouse_button_callback(GLFWwindow*, int button, int action, int);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double, double yoffset);
void setCallbackFunction(GLFWwindow* mWindow);

void showFPS(void);
float deltaTime = 0.0f;

int main(int argc, char * argv[]) {
	GLFWwindow* mWindow = createWindow();
	if (mWindow == nullptr) {
		fprintf(stderr, "Failed to Create OpenGL Context");
		return EXIT_FAILURE;
	}

	// Create Context and Load OpenGL Functions
	glfwMakeContextCurrent(mWindow);
	gladLoadGL();
	fprintf(stderr, "OpenGL %s\n", glGetString(GL_VERSION));

	// Set the required callback functions
	setCallbackFunction(mWindow);

	// Change Viewport
	int width, height;
	glfwGetFramebufferSize(mWindow, &width, &height);
	glViewport(0, 0, width, height);

	// Setup shaders
	// ----------------------------------------
	s = new Shader("Shaders\\color.vert", "Shaders\\color.frag", 0, 0, 0);
	s->compile();

	texture = new Shader("Shaders\\texture.vert", "Shaders\\texture.frag", 0, 0, 0);
	texture->compile();

	lightning = new Shader("Shaders\\light.vert", "Shaders\\light.frag", 0, 0, 0);
	lightning->compile();

	light_transp = new Shader("Shaders\\light_transp.vert", "Shaders\\light_transp.frag", 0, 0, 0);
	light_transp->compile();

	mapTexture = new Shader("Shaders\\cubeMap.vert", "Shaders\\cubeMap.frag", 0, 0, 0);
	mapTexture->compile();

	shadow = new Shader("Shaders\\depthShader.vert", "Shaders\\depthShader.frag", "Shaders\\depthShader.geom", 0, 0);
	shadow->compile();

	// Setup Models
	// ----------------------------------------
	Pin = new PinModel();
	const int nbPins = Pin->getNbPins();
	Ball = new BallModel();
	Room = new RoomModel();
	Track = new TrackModel();
	EndTrack = new EndTrackModel();
	Lamp = new LampModel();

	powerBar = new PowerBar();
	directionBar = new DirectionBar();

	map = new Cube();
	//Texture pour la map
	GLuint ourMapTexture = cubeMapTexture();

	// Set up the projection matrix
	ProjectionMatrix = glm::perspective(glm::radians(90.0f), 4.0f / 3.0f, 0.1f, 100.0f);

	// Set up the camera matrix
	cam = new Camera(Ball->cameraCurPos, cameraUp);

	glEnable(GL_DEPTH_TEST);
	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// configure depth map FBO
	// -----------------------
	unsigned int depthMapFBO;
	glGenFramebuffers(1, &depthMapFBO);
	float near_plane = 0.1f;
	float far_plane = 35.0f;

	// create depth cubemap texture
	unsigned int depthCubemap;
	glGenTextures(1, &depthCubemap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubemap);
	for (unsigned int i = 0; i < 6; ++i)
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	// attach depth texture as FBO's depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubemap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Initialization of the scene
	// ----------------------------------------
	ModelBall = glm::translate(glm::mat4(1.0f), Ball->initialPosition);
	int updateDirectionBar = 0;
	initPins(nbPins, lightning, cam, ProjectionMatrix, far_plane, depthCubemap);
	initBullet(nbPins);

	while (glfwWindowShouldClose(mWindow) == false) {

		showFPS();
		m_pWorld->stepSimulation(1.f / 120.0f); //framerate

		// Background Fill Color
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// 1. render depth of scene to texture (from light's perspective)
		// --------------------------------------------------------------
		renderDepthMap(shadow, depthMapFBO, near_plane, far_plane);

		// reset Viewport
		// --------------------------------------------------------------
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Render the scene as normal way
		// --------------------------------------------------------------
		if (showPowerBar) {
			//Display powerBar
			glm::mat4 ModelPB = glm::translate(glm::mat4(1.0f), glm::vec3(-3.0f, 0.5f, 6.0f)) * glm::scale(glm::mat4(1.0f), glm::vec3(0.05f, 0.1f, 1.0f));
			powerBar->show(s, ModelPB, cam->GetViewMatrix(), ProjectionMatrix);

			if (!keys[GLFW_MOUSE_BUTTON_RIGHT] && setPowerBar) {
				showPowerBar = false;
				Ball->MovementSpeed = powerBar->getPower() + 10;
			}
		}
		else {
			if (!startMovingBall)
			{
				glm::mat4 ModelDB = glm::translate(glm::mat4(1.0f), glm::vec3(-2.0f, 1.5f, 6.0f)) * glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 1.0f));
				updateDirectionBar++;
				if (updateDirectionBar == directionBarDifficulty)
				{
					updateDirectionBar = 0;
					directionBar->updateDirection();
				}
				directionBar->show(s, ModelDB, cam->GetViewMatrix(), ProjectionMatrix);
			}
		}

		// Render the Skybox
		renderSkybox(ourMapTexture);

		// Display the Room
		glm::mat4 ModelViewRoom = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f));
		Room->showWithLight(lightning, ModelViewRoom, cam, ProjectionMatrix, far_plane, depthCubemap);

		// Display the Track
		glm::mat4 ModelViewTrack = glm::translate(glm::mat4(1.0f), Track->initialPosition);
		Track->showWithLight(lightning, ModelViewTrack, cam, ProjectionMatrix, far_plane, depthCubemap);
		Track->DrawSecondaryTracks(lightning, cam, ProjectionMatrix, far_plane, depthCubemap);

		// Display the end of Track
		glm::mat4 ModelViewEndTrack = glm::translate(glm::mat4(1.0f), EndTrack->initialPosition);
		EndTrack->showWithLight(lightning, ModelViewEndTrack, cam, ProjectionMatrix, far_plane, depthCubemap);
		EndTrack->DrawSecondaryTracks(lightning, cam, ProjectionMatrix, far_plane, depthCubemap);

		// Display the Ball
		if (startMovingBall && (Ball->currentPosition.z > Ball->finalzPos)) {
			moveBall();
			checkCollision();
		}

		// Display the Ball
		if (ball_transparency) {
			Ball->showWithLight(light_transp, ModelBall, cam, ProjectionMatrix, far_plane, depthCubemap);
		}
		else {
			Ball->showWithLight(lightning, ModelBall, cam, ProjectionMatrix, far_plane, depthCubemap);
		}

		// Display the pins
		updatePins(nbPins, lightning, cam, ProjectionMatrix, far_plane, depthCubemap);

		// Display the lamp
		glm::mat4 LampViewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(lightPos.x, 6.9f, lightPos.z));
		Lamp->showWithLight(texture, LampViewMatrix, cam, ProjectionMatrix);

		// Flip Buffers and Draw
		glfwSwapBuffers(mWindow);
		glfwPollEvents();
	}
	cleanWord();
	glfwTerminate();
	return EXIT_SUCCESS;
}

void renderSkybox(GLuint ourMapTexture)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, ourMapTexture);
	mapTexture->use();
	mapTexture->setInteger("cubeMap", 0);
	glm::mat4 ModelViewMap = glm::scale(glm::mat4(1.0f), glm::vec3(50.0f));
	map->show(mapTexture, ModelViewMap, cam->GetViewMatrix(), ProjectionMatrix);
}

void showFPS(void)
{
	static double lastTime = glfwGetTime();
	static int nbFrames = 0;

	// Measure speed
	double currentTime = glfwGetTime();
	deltaTime = currentTime - lastTime;
	nbFrames++;
	if (currentTime - lastTime >= 1.0) { // If last prinf() was more than 1 sec ago
										 // printf and reset timer
		std::cout << 1000.0 / double(nbFrames) << " ms/frame -> " << nbFrames << " frames/sec" << std::endl;
		nbFrames = 0;
		lastTime += 1.0;
	}
}

//key and mouse manager
void key_callback(GLFWwindow* window, int key, int, int action, int)
{
	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;

	if (keys[GLFW_KEY_ESCAPE])
		glfwSetWindowShouldClose(window, GL_TRUE);

	// V-SYNC
	if (keys[GLFW_KEY_U]) {
		static bool vsync = true;
		if (vsync) {
			glfwSwapInterval(1);
		}
		else {
			glfwSwapInterval(0);
		}
		vsync = !vsync;
	}

	if (keys[GLFW_KEY_SPACE] && !showPowerBar) {
		startMovingBall = true;
		Ball->ballDirection = (directionBar->getDirection() * 10) * (Ball->MovementSpeed / 100);
		cam->reset(Ball->cameraCurPos, cameraUp);
	}

	if (keys[GLFW_KEY_R] && startMovingBall) {
		restartGame();
	}

	if (keys[GLFW_KEY_T]) {
		ball_transparency = !ball_transparency;
	}
}

void mouse_button_callback(GLFWwindow*, int button, int action, int) {
	if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_RIGHT] = true;
	else
		keys[GLFW_MOUSE_BUTTON_RIGHT] = false;

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_LEFT] = true;
	else
		keys[GLFW_MOUSE_BUTTON_LEFT] = false;

	if (button == GLFW_MOUSE_BUTTON_MIDDLE && action == GLFW_PRESS)
		keys[GLFW_MOUSE_BUTTON_MIDDLE] = true;
	else
		keys[GLFW_MOUSE_BUTTON_MIDDLE] = false;
}

double currentXPos = 0;
double currentYPos = 0;
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (keys[GLFW_MOUSE_BUTTON_RIGHT]) {
		setPowerBar = true;
		if (ypos > powerBar->powerBarcPos) {
			powerBar->decreasePower();
		}
		else {
			powerBar->increasePower();
		}
		powerBar->powerBarcPos = ypos;
	}

	if (keys[GLFW_KEY_LEFT_CONTROL])
	{
		if (ypos > currentYPos)
			cam->ProcessKeyboard(FORWARD, 1.0f);
		else if (ypos < currentYPos)
			cam->ProcessKeyboard(BACKWARD, 1.0f);

		if (xpos > currentXPos)
			cam->ProcessKeyboard(LEFT, 1.0f);
		else if (xpos < currentXPos)
			cam->ProcessKeyboard(RIGHT, 1.0f);

		currentXPos = xpos;
		currentYPos = ypos;
	}

	if (keys[GLFW_KEY_LEFT_SHIFT])
	{
		int up = 0;
		int left = 0;

		if (ypos > currentYPos)
			up = -10;
		else if (ypos < currentYPos)
			up = 10;

		if (xpos > currentXPos)
			left = 10;
		else if (xpos < currentXPos)
			left = -10;

		cam->ProcessMouseMovement(left, up);
		currentXPos = xpos;
		currentYPos = ypos;
	}

}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	if (yoffset > 0)
		cam->ProcessKeyboard(FORWARD, 10.0f);
	else
		cam->ProcessKeyboard(BACKWARD, 10.0f);
}

void setCallbackFunction(GLFWwindow* mWindow)
{
	// Set the required callback functions
	glfwSetKeyCallback(mWindow, key_callback);
	glfwSetCursorPosCallback(mWindow, mouse_callback);
	glfwSetMouseButtonCallback(mWindow, mouse_button_callback);
	glfwSetScrollCallback(mWindow, scroll_callback);
}

GLuint static createTexture(std::string filePath) {
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); 	// Set texture wrapping to GL_REPEAT
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);  // Set texture filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // Set texture filtering
	int width, height;
	unsigned char *imageFile = stbi_load(filePath.c_str(), &width, &height, 0, 0); // loads the image
																				   // ... replace '0' with '1'..'4' to force that many components per pixel (eg: you want 3 -> RGB)
																				   // ... but 'n' will always be the number that it would have been if you said 0
																				   // send the image
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageFile);
	glGenerateMipmap(GL_TEXTURE_2D); // generate the mipmaps
	stbi_image_free(imageFile); // we don't need anymore the image data
	glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidently mess up our texture.

	return texture;
}

GLuint cubeMapTexture() {
	std::vector<std::string> textures = { "texture\\cubeMap\\posx.jpg", "texture\\cubeMap\\negx.jpg", "texture\\cubeMap\\posy.jpg", "texture\\cubeMap\\negy.jpg",
											"texture\\cubeMap\\posz.jpg", "texture\\cubeMap\\negz.jpg" }; // Must be 6 images
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	int width, height;
	unsigned char* image;
	for (GLuint i = 0; i < textures.size(); i++) {
		image = stbi_load(textures[i].c_str(), &width, &height, 0, 0);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		stbi_image_free(image);
	}
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	return texture;
}

GLFWwindow* createWindow() {
	// Load GLFW and Create a Window
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	GLFWwindow* mWindow = glfwCreateWindow(mWidth, mHeight, "INFO-H-502 - LOLO Land", nullptr, nullptr);

	// Check for Valid Context
	if (mWindow == nullptr)
		return nullptr;

	// Create Context and Load OpenGL Functions
	glfwMakeContextCurrent(mWindow);
	gladLoadGL();
	fprintf(stderr, "OpenGL %s\n", glGetString(GL_VERSION));

	return mWindow;
}


void moveBall() {
	//Give impulsion to RB(rigidbody) Ball
	if (!giveImpulsion)
	{
		Ball->getRigidBody()->applyCentralImpulse(btVector3(Ball->ballDirection, 0, -Ball->MovementSpeed * 10));
		Ball->getRigidBody()->applyTorqueImpulse(btVector3(-90, 0, 0));
		Ball->getRigidBody()->activate();
		giveImpulsion = true;
	}

	//Get RB position
	btTransform  trans;
	btScalar matrix[16];
	Ball->getRigidBody()->getMotionState()->getWorldTransform(trans);
	trans.getOpenGLMatrix(matrix);
	ModelBall = glm::make_mat4(matrix);

	//Move camera
	btVector3 newPos = trans.getOrigin();
	Ball->currentPosition = glm::vec3(newPos.getX(), newPos.getY(), newPos.getZ());
	Ball->cameraCurPos.z = Ball->currentPosition.z + 1.0f;
	cam->setPosition(Ball->cameraCurPos);
}

void restartGame() {
	// Clear forces and velocity due to the impulsion
	btVector3 zero(0.0f, 0.0f, 0.0f);
	Ball->getRigidBody()->clearForces();
	Ball->getRigidBody()->setLinearVelocity(zero);
	Ball->getRigidBody()->setAngularVelocity(zero);

	// Reset the motionstate with the default initial
	btDefaultMotionState *ms = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(Ball->initialPosition.x, Ball->initialPosition.y, Ball->initialPosition.z)));
	Ball->getRigidBody()->setMotionState(ms);

	//Move physical Ball
	Ball->currentPosition = Ball->initialPosition;
	btTransform  trans;
	btScalar matrix[16];
	Ball->getRigidBody()->getMotionState()->getWorldTransform(trans);
	trans.getOpenGLMatrix(matrix);
	ModelBall = glm::make_mat4(matrix);

	// Reset the pins to the default location
	int nbPins = 0;
	for (int i = 0; i < Pin->getNbPins(); i++)
	{
		Pin->isCollide[i] = PinCollision[i];
		if (Pin->isCollide[i]) {
			m_pWorld->removeRigidBody(Pin->getRigidBody(i));
		}
		else
		{
			nbPins++;
			Pin->getRigidBody(i)->clearForces();
			Pin->getRigidBody(i)->setLinearVelocity(zero);
			Pin->getRigidBody(i)->setAngularVelocity(zero);
			btDefaultMotionState *ms = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(Pin->initialPositions[i].x, Pin->initialPositions[i].y, Pin->initialPositions[i].z)));
			Pin->getRigidBody(i)->setMotionState(ms);
		}
	}

	// If we have no more pins
	if (nbPins == 0) {
		for (int i = 0; i < Pin->getNbPins(); i++) {
			Pin->isCollide[i] = false;
			PinCollision[i] = false;
			Pin->getRigidBody(i)->clearForces();
			Pin->getRigidBody(i)->setLinearVelocity(zero);
			Pin->getRigidBody(i)->setAngularVelocity(zero);
			btDefaultMotionState *ms = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(Pin->initialPositions[i].x, Pin->initialPositions[i].y, Pin->initialPositions[i].z)));
			Pin->getRigidBody(i)->setMotionState(ms);
			m_pWorld->addRigidBody(Pin->getRigidBody(i));
		}
	}

	//Move camera
	Ball->cameraCurPos = Ball->cameraInitPos;
	cam->reset(Ball->cameraCurPos, cameraUp);

	//power
	Ball->MovementSpeed = 0;
	powerBar->resetPower();
	Ball->ballDirection = 0;
	directionBar->resetDirection();
	showPowerBar = true;
	setPowerBar = false;
	startMovingBall = false;
	giveImpulsion = false;
}

void initBullet(int nbPins) {
	btBroadphaseInterface* m_pBroadphase;
	btCollisionConfiguration* m_pCollisionConfiguration;
	btCollisionDispatcher* m_pDispatcher;
	btConstraintSolver* m_pSolver;

	m_pCollisionConfiguration = new btDefaultCollisionConfiguration();
	m_pDispatcher = new btCollisionDispatcher(m_pCollisionConfiguration);
	m_pBroadphase = new btDbvtBroadphase();
	m_pSolver = new btSequentialImpulseConstraintSolver();
	m_pWorld = new btDiscreteDynamicsWorld(m_pDispatcher, m_pBroadphase, m_pSolver, m_pCollisionConfiguration);

	m_pWorld->setGravity(btVector3(0, -10, 0));
	m_pWorld->addRigidBody(Track->getRigidBodyTrack());
	m_pWorld->addRigidBody(EndTrack->getRigidBodyEndTrack());
	for (size_t i = 0; i < nbPins; i++)
		m_pWorld->addRigidBody(Pin->getRigidBody(i));
	m_pWorld->addRigidBody(Ball->getRigidBody());
}

void initPins(int nbPins, Shader* s, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthCubemap) {
	glm::vec3 first = Pin->initialPosition;
	glm::mat4 model;

	float space_x = 0.6f;
	int num = 0;
	for (int i = 0; i < 4; i++) {
		float shift_z = 0.3f * i;
		float shift_x = 0.3f * i;
		for (int j = 0; j < (i + 1); j++) {
			Pin->addRigidBody(num, btVector3(first.x + shift_x, first.y, first.z - shift_z));
			Pin->initialPositions[num] = glm::vec3(first.x + shift_x, first.y, first.z - shift_z);
			model = glm::translate(glm::mat4(1.0f), glm::vec3(first.x + shift_x, first.y, first.z - shift_z));
			Pin->showWithLight(s, model, cam, projection, far_plane, depthCubemap);
			shift_x -= space_x;
			num++;
		}
	}
}

void updatePins(int nbPins, Shader* s, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthCubemap) {
	btTransform  trans;
	glm::mat4    model;
	btScalar matrix[16];
	for (int i = 0; i < nbPins; i++)
	{
		if (!Pin->isCollide[i]) {
			Pin->getRigidBody(i)->getMotionState()->getWorldTransform(trans);
			trans.getOpenGLMatrix(matrix);
			model = glm::make_mat4(matrix);
			Pin->showWithLight(s, model, cam, projection, far_plane, depthCubemap);
		}
	}
}

void checkCollision()
{
	int numManifolds = m_pWorld->getDispatcher()->getNumManifolds();
	for (int i = 0; i < numManifolds; i++) {
		btPersistentManifold* contactManifold = m_pWorld->getDispatcher()->getManifoldByIndexInternal(i);
		const btCollisionObject* obA = contactManifold->getBody0();
		const btCollisionObject* obB = contactManifold->getBody1();

		int numContacts = contactManifold->getNumContacts();
		if (numContacts > 0) {
			// if one of the object is the ball
			if (obA == Ball->getRigidBody() || obB == Ball->getRigidBody()) {
				// check if the ball has a collision with a/some pin(s)
				for (int i = 0; i < Pin->getNbPins(); i++) {
					if (Pin->getRigidBody(i) == obA || Pin->getRigidBody(i) == obB) {
						PinCollision[i] = true;
					}
				}
			}
			else {
				// if there is a contact/collision between pins
				for (int i = 0; i < Pin->getNbPins(); i++) {
					if (Pin->getRigidBody(i) == obA || Pin->getRigidBody(i) == obB) {
						for (int j = 0; j < Pin->getNbPins(); j++) {
							if (j != i) {
								if (Pin->getRigidBody(j) == obA || Pin->getRigidBody(j) == obB) {
									PinCollision[i] = true;
									PinCollision[j] = true;
								}
							}
						}
					}
				}
			}
		}
	}
}

void cleanWord() {
	for (int i = m_pWorld->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject*	obj = m_pWorld->getCollisionObjectArray()[i];
		btRigidBody*		body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
			delete body->getMotionState();
		m_pWorld->removeRigidBody(body);
		delete obj;
	}
}


void renderDepthMap(Shader *s, unsigned int depthMapFBO, float near_plane, float far_plane)
{
	float aspect = (float)SHADOW_WIDTH / (float)SHADOW_HEIGHT;
	glm::mat4 shadowProj = glm::perspective(glm::radians(90.0f), aspect, near_plane, far_plane);
	std::vector<glm::mat4> shadowTransforms;
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms.push_back(shadowProj *
		glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));

	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	// Display the Room
	glm::mat4 ModelViewRoom = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f));
	Room->renderShadow(s, ModelViewRoom, shadowTransforms, far_plane);

	// Display the Track
	glm::mat4 ModelViewTrack = glm::translate(glm::mat4(1.0f), Track->initialPosition);
	Track->renderShadow(s, ModelViewTrack, shadowTransforms, far_plane);

	// Display the end of Track
	glm::mat4 ModelViewEndTrack = glm::translate(glm::mat4(1.0f), EndTrack->initialPosition);
	EndTrack->renderShadow(s, ModelViewEndTrack, shadowTransforms, far_plane);

	ModelBall = glm::translate(glm::mat4(1.0f), Ball->currentPosition);
	Ball->renderShadow(s, ModelBall, shadowTransforms, far_plane);

	// Display the pins
	btTransform  trans;
	glm::mat4    model;
	btScalar matrix[16];
	for (int i = 0; i < Pin->getNbPins(); i++)
	{
		if (!Pin->isCollide[i]) {
			Pin->getRigidBody(i)->getMotionState()->getWorldTransform(trans);
			trans.getOpenGLMatrix(matrix);
			model = glm::make_mat4(matrix);
			Pin->renderShadow(s, model, shadowTransforms, far_plane);
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}