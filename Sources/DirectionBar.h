#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Shader.hpp"
class DirectionBar {
private:
	void construct();
	GLuint VAO;
	int direction;
	int setAmount = 1;
	int currentDirectPos = 0;
	GLfloat initVertices[36];
public:
	DirectionBar();
	~DirectionBar();
	void show(Shader *s, glm::mat4 model, glm::mat4 camera, glm::mat4 projection);
	void updateDirection();
	int getDirection();
	void resetDirection();
};
