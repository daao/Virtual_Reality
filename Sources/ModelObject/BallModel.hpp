#pragma once
#include "../glitter.hpp"

// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include "../Model.hpp"
#include "../Shader.hpp"
#include "../Util.hpp"

#include "bullet\btBulletCollisionCommon.h"
#include "bullet\btBulletDynamicsCommon.h"

class BallModel {

private:
	Mesh ball;
	Mesh fingerhole;
	btRigidBody *rigidBody;

public:
	GLfloat Yaw;
	GLfloat Pitch;
	GLfloat MovementSpeed;
	GLfloat ballDirection;
	GLfloat ballAngle = 0.0f;
	btTransform  initialTransfor;

	//Ball position
	const glm::vec3 initialPosition = glm::vec3(-2.0f, 0.6f, 6.0f);
	glm::vec3 currentPosition = initialPosition;
	GLfloat finalzPos = -8.0f;

	//Camera position
	const glm::vec3 cameraInitPos = glm::vec3(-2.0f, 1.0f, 7.0f);
	glm::vec3 cameraCurPos = glm::vec3(-2.0f, 1.0f, 7.0f);
	

	BallModel()
	{
		Model *bowling = new Model("Objects\\ball.obj");
		ball = bowling->getMesh(0);
		createRigidBody();
	}

	Mesh getMesh() {
		return ball;
	}

	btRigidBody* getRigidBody() {
		return rigidBody;
	}

	void renderShadow(Shader *s, glm::mat4 model, std::vector<glm::mat4> shadowTransforms, float far_plane)
	{
		s->use();
		s->setMatrix4("model", model);
		for(int i = 0; i < shadowTransforms.size(); i++){
			std::string str = "shadowMatrices[" + std::to_string(i) + "]";
			s->setMatrix4(str.c_str(), shadowTransforms[i]);
		}
		s->setVector3f("lightPos", lightPos);
		s->setFloat("far_plane", far_plane);
		ball.Draw(*s);

	}

	void showWithLight(Shader *s, glm::mat4 model, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthCubemap) {
		s->use();
		s->setMatrix4("M", model);
		s->setMatrix4("C", cam->GetViewMatrix());
		s->setMatrix4("Pr", projection);
		s->setVector3f("cameraPos", cam->Position);

		// Light parameters
		s->lightSettings();

		//Material parameters
		s->setInteger("material.diffuse", 0);
		s->setVector3f("material.specular", glm::vec3(0.5f, 0.5f, 0.5f));
		s->setFloat("material.shininess", 64.0f);

		//Shadow parameters
		s->setFloat("far_plane", far_plane);
		s->setInteger("depthMap", 1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubemap);

		ball.Draw(*s);
	}

	void createRigidBody() {
		btConvexHullShape* shape = new btConvexHullShape();
		btVector3 ver;
		
		for (size_t i = 0; i < ball.vertices.size(); i++)
		{
			ver = btVector3(ball.vertices[i].Position.x, ball.vertices[i].Position.y, ball.vertices[i].Position.z);
			shape->addPoint(ver);
		}

		btCollisionShape* shapeCollision = (btCollisionShape*) shape;

		btVector3 localInertia;
		btScalar mass = 50.0f;
		shapeCollision->calculateLocalInertia(mass, localInertia);

		initialTransfor.setIdentity();
		initialTransfor.setOrigin(btVector3(initialPosition.x, initialPosition.y, initialPosition.z));

		btDefaultMotionState *ballMotionState = new btDefaultMotionState(initialTransfor);
		btRigidBody::btRigidBodyConstructionInfo myBoxRigidBodyConstructionInfo(mass, ballMotionState, shapeCollision, localInertia);

		rigidBody = new btRigidBody(myBoxRigidBodyConstructionInfo);
		rigidBody->setUserPointer(this);
		rigidBody->setRestitution(0.6f);
		rigidBody->setFriction(.1f);
		rigidBody->setRollingFriction(.2f);
	}
};