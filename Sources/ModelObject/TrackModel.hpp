#pragma once
#include "../glitter.hpp"

// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include "../Model.hpp"
#include "../Shader.hpp"
#include "../Util.hpp"
#include "../Camera.hpp"

#include "bullet/btBulletCollisionCommon.h"
#include "bullet/btBulletDynamicsCommon.h"

class TrackModel {

private:
	Mesh track;
	btRigidBody *rigidBodyTrack;
	
	void Draw(Shader *s)
	{
		track.Draw(*s);
	}

public:
	const glm::vec3 initialPosition = glm::vec3(-2.0f, 0.2f, -0.5f);

	TrackModel()
	{
		Model *bowling = new Model("Objects\\track.obj");
		track = bowling->getMesh(0);
		createRigidBodies();
	}

	void DrawSecondaryTracks(Shader *s, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthMap)
	{
		float shift = 3.0f;
		glm::vec3 newPosition = glm::vec3(initialPosition.x - shift, initialPosition.y, initialPosition.z);
		glm::mat4 model = glm::translate(glm::mat4(1.0f), newPosition);
		showWithLight(s, model, cam, projection, far_plane, depthMap);

		shift = 2.9f;
		newPosition = glm::vec3(newPosition.x - shift, newPosition.y, newPosition.z);
		model = glm::translate(glm::mat4(1.0f), newPosition);
		showWithLight(s, model, cam, projection, far_plane, depthMap);
	}

	void renderShadow(Shader *s, glm::mat4 model, std::vector<glm::mat4> shadowTransforms, float far_plane)
	{
		s->use();
		s->setMatrix4("model", model);
		for (int i = 0; i < shadowTransforms.size(); i++) {
			std::string str = "shadowMatrices[" + std::to_string(i) + "]";
			s->setMatrix4(str.c_str(), shadowTransforms[i]);
		}
		s->setVector3f("lightPos", lightPos);
		s->setFloat("far_plane", far_plane);
		Draw(s);
	}

	void showWithLight(Shader *s, glm::mat4 model, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthCubemap) {
		s->use();
		s->setMatrix4("M", model);
		s->setMatrix4("C", cam->GetViewMatrix());
		s->setMatrix4("Pr", projection);
		s->setVector3f("cameraPos", cam->Position);

		//Shadow parameters
		s->setFloat("far_plane", far_plane);
		s->setInteger("depthMap", 1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubemap);

		// Light parameters
		s->lightSettings();

		//Material parameters
		s->setInteger("material.diffuse", 0);
		s->setVector3f("material.specular", glm::vec3(0.5f, 0.5f, 0.5f));
		s->setFloat("material.shininess", 32.0f);
		Draw(s);
	}

	Mesh getMesh() {
		return track;
	}

	btRigidBody* getRigidBodyTrack() {
		return rigidBodyTrack;
	}

	btCollisionShape* createShape(Mesh mesh) {
		btConvexHullShape* shape = new btConvexHullShape();
		btVector3 ver;
		for (size_t i = 0; i < track.vertices.size(); i++)
		{
			ver = btVector3(track.vertices[i].Position.x, track.vertices[i].Position.y, track.vertices[i].Position.z);
			shape->addPoint(ver);
		}
		return (btCollisionShape*)shape;
	}

	btRigidBody * createRB(btVector3 position, btCollisionShape* shapeCollision) {
		btTransform transformer;
		transformer.setIdentity();
		transformer.setOrigin(position);
		btVector3 localInertia(0,0,0);
		btScalar mass = 0.0f;

		btDefaultMotionState *ballMotionState = new btDefaultMotionState(transformer);
		btRigidBody::btRigidBodyConstructionInfo myBoxRigidBodyConstructionInfo(mass, ballMotionState, shapeCollision, localInertia);
		return new btRigidBody(myBoxRigidBodyConstructionInfo);
	}

	void createRigidBodies() {
		btCollisionShape* shapeCollision = createShape(track);
		rigidBodyTrack  = createRB(btVector3(initialPosition.x, initialPosition.y, initialPosition.z), createShape(track));
		rigidBodyTrack->setUserPointer(this);
		rigidBodyTrack->setRestitution(0.3f);
		rigidBodyTrack->setFriction(1.0f);
		rigidBodyTrack->setRollingFriction(1.0f);
	}
};