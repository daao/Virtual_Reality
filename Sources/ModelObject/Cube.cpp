#include "Cube.hpp"

// TODO : FAUT AJOUTER LE CODE DES TEXTURES (LE RENDRE OPTIONNEL)


GLfloat coordTexture[] = {
	0, 0,   1, 0,   1, 1,
	0, 0,   0, 1,   1, 1,

	0, 0,   1, 0,   1, 1,
	0, 0,   0, 1,   1, 1,

	0, 0,   1, 0,   1, 1,
	0, 0,   0, 1,   1, 1,

	0, 0,   1, 0,   1, 1,
	0, 0,   0, 1,   1, 1,

	0, 0,   1, 0,   1, 1,
	0, 0,   0, 1,   1, 1,

	0, 0,   1, 0,   1, 1,
	0, 0,   0, 1,   1, 1
};

Cube::Cube()
{
	this->construct();
}


Cube::Cube(glm::vec3 rgb)
{
	this->setColors(rgb);
	this->construct();
}


Cube::~Cube()
{
}


void Cube::construct() {
	GLuint VBO, VBOColor, VBONormal, VBOTexture;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &VBOColor); // second VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBOColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &VBONormal); // third VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBONormal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(2);

	glGenBuffers(1, &VBOTexture); // 4th VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBOTexture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(coordTexture), coordTexture, GL_STATIC_DRAW);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(3);

	glBindVertexArray(0); // Unbind the VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind the VBO
}

void Cube::setColors(glm::vec3 rgb) {
	int i;
	for (i = 0; i < sizeof(this->colors)/sizeof(GLfloat); i++) {
		if (i % 3 == 0) {
			this->colors[i] = rgb.x;
		}
		else if (i % 3 == 1) {
			this->colors[i] = rgb.y;
		}
		else {
			this->colors[i] = rgb.z;
		}

	}
}


void Cube::show(Shader *s, glm::mat4 model, glm::mat4 camera, glm::mat4 projection) {
	s->use();
	s->setMatrix4("M", model);
	s->setMatrix4("C", camera);
	s->setMatrix4("Pr", projection);
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void Cube::showWithLight(Shader *s, glm::mat4 model, glm::mat4 view, glm::mat4 projection, GLuint texture) {
	s->use();
	s->setMatrix4("M", model);
	s->setMatrix4("C", view);
	s->setMatrix4("Pr", projection);
	s->setVector3f("cameraPos", cameraPos);

	// Light parameters
	//s->setVector3f("lightPos", lightPos);
	s->setVector3f("light.position", lightPos);
	s->setVector3f("light.ambient", glm::vec3(0.3f, 0.3f, 0.3f));
	s->setVector3f("light.diffuse", glm::vec3(0.7f, 0.7f, 0.7f)); // darken the light a bit to fit the scene
	s->setVector3f("light.specular", glm::vec3(0.9f, 0.9f, 0.9f));

	s->setFloat("light.linear", 0.007f);
	s->setFloat("light.quadratic", 0.0002f);

	//Material parameters
	if (texture != NULL) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		s->setInteger("material.diffuse", 0);
	} else {
		s->setVector3f("material.diffuse", glm::vec3(colors[0], colors[1], colors[2]));
	}

	s->setVector3f("material.specular", glm::vec3(1.0f, 1.0f, 1.0f));
	s->setFloat("material.shininess", 32.0f);

	// TODO : faut activer les textures
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}
