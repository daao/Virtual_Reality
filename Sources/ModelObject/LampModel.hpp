#pragma once
#include "../glitter.hpp"

// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include "../Model.hpp"
#include "../Shader.hpp"
#include "../Util.hpp"

#include "bullet\btBulletCollisionCommon.h"
#include "bullet\btBulletDynamicsCommon.h"

class LampModel {

private:
	Model *lamp;

public:
	LampModel()
	{
		lamp = new Model("Objects\\lamp.obj");
	}
	
	void showWithLight(Shader *s, glm::mat4 model, Camera *cam, glm::mat4 projection) {
		s->use();
		s->setMatrix4("M", model);
		s->setMatrix4("C", cam->GetViewMatrix());
		s->setMatrix4("Pr", projection);

		//Material parameters
		s->setInteger("ourTexture", 0);

		lamp->Draw(*s);
	}
};