#pragma once
#include "../glitter.hpp"

// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include "../Model.hpp"
#include "../Shader.hpp"
#include "../Util.hpp"

class RoomModel {

private:

	Model *room;
	Model *hole;

	void Draw(Shader *s)
	{
		room->Draw(*s);
		hole->Draw(*s);
	}

public:
	Mesh main_hole_left;
	Mesh main_hole_right;

	RoomModel()
	{
		room = new Model("Objects\\env.obj");

		hole = new Model("Objects\\hole.obj");
		main_hole_left = hole->getMesh(0);
		main_hole_right = hole->getMesh(2);
	}

	void renderShadow(Shader *s, glm::mat4 model, std::vector<glm::mat4> shadowTransforms, float far_plane)
	{
		s->use();
		s->setMatrix4("model", model);
		for (int i = 0; i < shadowTransforms.size(); i++) {
			std::string str = "shadowMatrices[" + std::to_string(i) + "]";
			s->setMatrix4(str.c_str(), shadowTransforms[i]);
		}
		s->setVector3f("lightPos", lightPos);
		s->setFloat("far_plane", far_plane);
		Draw(s);
	}

	void showWithLight(Shader *s, glm::mat4 model, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthCubemap) {
		s->use();
		s->setMatrix4("M", model);
		s->setMatrix4("C", cam->GetViewMatrix());
		s->setMatrix4("Pr", projection);
		s->setVector3f("cameraPos", cam->Position);

		// Light parameters
		s->lightSettings();

		//Material parameters
		s->setInteger("material.diffuse", 0);
		s->setVector3f("material.specular", glm::vec3(0.5f, 0.5f, 0.5f));
		s->setFloat("material.shininess", 32.0f);

		//Shadow parameters
		s->setFloat("far_plane", far_plane);
		s->setInteger("depthMap", 1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubemap);

		Draw(s);
	}
};