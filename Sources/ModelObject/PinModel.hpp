#pragma once
#include "glitter.hpp"

// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#include "Model.hpp"
#include "Shader.hpp"
#include "../Util.hpp"

#include "bullet\btBulletCollisionCommon.h"
#include "bullet\btBulletDynamicsCommon.h"

class PinModel {

private:
	Mesh pin;
	Mesh rings;
	static const int nbPins = 10;
	btRigidBody* rigidBody[nbPins];
	btCollisionShape* shapeCollision;
	btVector3 localInertia = btVector3(0, 0, 0);
	btScalar mass = 1.0f;

	void Draw(Shader *s) {
		pin.Draw(*s);
		rings.Draw(*s);
	}

public:
	bool isCollide[nbPins];
	glm::vec3 initialPositions[nbPins];
	glm::vec3 currentPositions[nbPins];
	glm::vec3 initialPosition = glm::vec3(-2.0f, 0.6f, -6.0f);
	PinModel()
	{
		Model *bowling = new Model("Objects\\pin.obj");
		pin = bowling->getMesh(0);
		rings = bowling->getMesh(1);
		createShape();
	}

	void renderShadow(Shader *s, glm::mat4 model, std::vector<glm::mat4> shadowTransforms, float far_plane)
	{
		s->use();
		s->setMatrix4("model", model);
		for (int i = 0; i < shadowTransforms.size(); i++) {
			std::string str = "shadowMatrices[" + std::to_string(i) + "]";
			s->setMatrix4(str.c_str(), shadowTransforms[i]);
		}
		s->setVector3f("lightPos", lightPos);
		s->setFloat("far_plane", far_plane);
		Draw(s);

	}

	void showWithLight(Shader *s, glm::mat4 model, Camera *cam, glm::mat4 projection, float far_plane, unsigned int depthCubemap) {
		s->use();
		s->setMatrix4("M", model);
		s->setMatrix4("C", cam->GetViewMatrix());
		s->setMatrix4("Pr", projection);
		s->setVector3f("cameraPos", cam->Position);

		// Light parameters
		s->lightSettings();

		//Material parameters
		s->setInteger("material.diffuse", 0);
		s->setVector3f("material.specular", glm::vec3(0.5f, 0.5f, 0.5f));
		s->setFloat("material.shininess", 64.0f);

		//Shadow parameters
		s->setFloat("far_plane", far_plane);
		s->setInteger("depthMap", 1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubemap);

		Draw(s);
	}

	btRigidBody* getRigidBody(int i) {
		return rigidBody[i];
	}

	int getNbPins() {
		return nbPins;
	}

	void createShape() {
		btConvexHullShape* shape = new btConvexHullShape();
		btVector3 ver;
		for (size_t i = 0; i < pin.vertices.size(); i++)
		{
			ver = btVector3(pin.vertices[i].Position.x, pin.vertices[i].Position.y, pin.vertices[i].Position.z);
			shape->addPoint(ver);
		}
		shapeCollision = (btCollisionShape*) shape;
		shapeCollision->calculateLocalInertia(mass, localInertia);
	}

	void addRigidBody(int i, btVector3 position) {
		btTransform transformer;
		transformer.setIdentity();
		transformer.setOrigin(position);
		
		btDefaultMotionState * motionState = new btDefaultMotionState(transformer);
		btRigidBody::btRigidBodyConstructionInfo myBoxRigidBodyConstructionInfo(mass, motionState, shapeCollision, localInertia);

		initialPositions[i] = glm::vec3(position.getX(), position.getY(), position.getZ());
		currentPositions[i] = initialPositions[i];
		rigidBody[i] = new btRigidBody(myBoxRigidBodyConstructionInfo);
	}
};