#pragma once
// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

#include <glm/gtc/matrix_transform.hpp>

#define white glm::vec3(1.0f, 1.0f, 1.0f)

class ColorsConst {
public:
	static glm::vec3 brown() { return glm::vec3(0.502f, 0.502f, 0.000f); }
	static glm::vec3 black() { return glm::vec3(0.0f, 0.0f, 0.0f); }
	static glm::vec3 royal_blue() { return glm::vec3(0.255, 0.412, 0.882); }
};