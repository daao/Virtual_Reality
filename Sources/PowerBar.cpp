#include "PowerBar.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>

PowerBar::PowerBar()
{
	power = 0;
}


PowerBar::~PowerBar()
{
}

GLfloat verticesPowerBar[] = {

	0.0f, 0.0f, 0.0f, //Pas modifier
	2.0f, 0.0f,  0.0f, //Pas modifier
	2.0f, 0.0f,  0.0f,

	0.0f, 0.0f, 0.0f, //Pas modifier
	0.0f, 0.0f, 0.0f,
	2.0f, 0.0f,  0.0f,

	0.0f, 10.0f, 0.0f, //Pas modifier
	0.0f, 0.0f, 0.0f,
	2.0f, 0.0f, 0.0f,

	0.0f, 10.0f, 0.0f, //Pas modifier
	2.0f, 10.0f, 0.0f, //Pas modifier
	2.0f, 0.0f, 0.0f
};



GLfloat colorsPowerBar[] = {
	255.0f, 0.0f, 0.0f,
	255.0f, 0.0f, 0.0f,
	255.0f, 0.0f, 0.0f,

	255.0f, 0.0f, 0.0f,
	255.0f, 0.0f, 0.0f,
	255.0f, 0.0f, 0.0f,

	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,

	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f
};


void PowerBar::show(Shader *s, glm::mat4 model, glm::mat4 camera, glm::mat4 projection) {
	construct();
	s->use();
	s->setMatrix4("M", model);
	s->setMatrix4("C", camera);
	s->setMatrix4("Pr", projection);
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void PowerBar::construct() {
	GLuint VBO, VBOColor, VBOTexture;;

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verticesPowerBar), verticesPowerBar, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &VBOColor); // second VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBOColor);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colorsPowerBar), colorsPowerBar, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0); // Unbind the VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0); // Unbind the VBO
}

void PowerBar::increasePower() {
	if (power < 100) {
		power = std::min(100, power + 2);
		updatePowerBar();
	}
}

void PowerBar::decreasePower() {
	if (power > 0) {
		power = std::max(0, power - 2);
		updatePowerBar();
	}
}

int PowerBar::getPower()
{
	return power;
}

void PowerBar::resetPower() {
	power = 0;
	updatePowerBar();
}

void PowerBar::updatePowerBar()
{
	// 10 est la taille de la barre
	// power est le poucentage courant de puissance
	int newPower = (10 * power) / 100;
	verticesPowerBar[7] = newPower;
	verticesPowerBar[13] = newPower;
	verticesPowerBar[16] = newPower;

	verticesPowerBar[22] = newPower;
	verticesPowerBar[25] = newPower;
	verticesPowerBar[34] = newPower;
}
