#pragma once
// System Headers
#include <glad/glad.h>
#include <GLFW/glfw3.h>

// Standard Headers
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>

// Camera parameters
#define cameraPos glm::vec3(0.0f, 5.0f, 5.0f)
#define cameraUp glm::vec3(0.0f, 1.0f, 0.0f)
#define cameraFront glm::vec3(0.0f, 0.0f, -1.0f)

// Light parameters
#define lightColor glm::vec3(1.0f, 1.0f, 1.0f)
#define lightPos glm::vec3(0.0f, 6.0f, -1.0f)
#define lightAmbient glm::vec3(0.2f, 0.2f, 0.2f)
#define lightDiffuse glm::vec3(0.5f, 0.5f, 0.5f)

// cover a distance of 600
#define lightLinear 0.007f
#define lightQuadratic 0.0002f

// Colors
#define white glm::vec3(1.0f, 1.0f, 1.0f)
#define black glm::vec3(0.0f, 0.0f, 0.0f)
#define brown glm::vec3(0.502f, 0.502f, 0.000f)
#define royal_blue glm::vec3(0.255f, 0.412f, 0.882f)
#define dark_orange glm::vec3(0.824f, 0.412f, 0.118f)

#define SHADOW_WIDTH 1024
#define SHADOW_HEIGHT 1024