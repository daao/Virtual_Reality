#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Shader.hpp"
class PowerBar {
private:
	void construct();
	GLuint VAO;
	int power;
	void updatePowerBar();
public:
	int powerBarcPos = 0;
	PowerBar();
	~PowerBar();
	void show(Shader *s, glm::mat4 model, glm::mat4 camera, glm::mat4 projection);
	void increasePower();
	void decreasePower();
	int getPower();
	void resetPower();
};
