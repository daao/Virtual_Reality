#version 330 core
layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;

uniform mat4 M;
uniform mat4 C;
uniform mat4 Pr;

out vec3 FragPos;
out vec3 aNormal;

void main(){
	gl_Position = Pr*C*M*vec4(position.xyz, 1.0f);
	FragPos = vec3(M * vec4(position.xyz, 1.0f));
	aNormal = vec3(M * vec4(normal.xyz, 0.0f)); 
}