#version 330 core
struct Material {
	sampler2D diffuse;
	vec3 specular;
	float shininess;
};
struct Light {
    vec3 position; 
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
	
	float linear; // used to reduce the intensity of near objects
	float quadratic; // used to reduce the intensity of far objects
};

in vec3 myColor;
in vec3 aNormal;
in vec3 FragPos;
in vec2 texCoord;
in vec4 FragPosShadow;

uniform Light light; 
uniform Material material;
uniform vec3 cameraPos;

uniform samplerCube depthMap;
uniform float far_plane;

out vec4 FragColor;

float ShadowCalculation(vec3 lightPos)
{
    // get vector between fragment position and light position
    vec3 fragToLight = FragPos - lightPos;

    // now get current linear depth as the length between the fragment and light position
    float currentDepth = length(fragToLight);
	
    // now test for shadows
	float shadow  = 0.0;
	float bias    = 0.5; 
	float samples = 4.0;
	float offset  = 0.1;
	for(float x = -offset; x < offset; x += offset / (samples * 0.5))
	{
		for(float y = -offset; y < offset; y += offset / (samples * 0.5))
		{
			for(float z = -offset; z < offset; z += offset / (samples * 0.5))
			{
				float closestDepth = texture(depthMap, fragToLight + vec3(x, y, z)).r; 
				closestDepth *= far_plane;   // Undo mapping [0;1]
				if(currentDepth - bias > closestDepth)
					shadow += 1.0;
			}
		}
	}
	shadow /= (samples * samples * samples);

    return shadow;
}

vec3 calculatePointLight(vec3 norm, vec3 viewDir)
{
	vec3 color = vec3(texture(material.diffuse, texCoord));
	// Calculation ambient
    vec3 ambient = color * light.ambient; 
    
	// Calculation diffuse
	vec3 lightDir = normalize(light.position - FragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * color * light.diffuse;
	
	// Calculation specular light
	vec3 halfwayDir = normalize(lightDir + viewDir); 
	float spec = pow(max(dot(viewDir, halfwayDir), 0.0), material.shininess);
	vec3 specular = material.specular * spec * light.specular;
	
	// Calculation point source light
	float distance    = length(light.position - FragPos);
	float attenuation = 1.0 / (1.0 + light.linear * distance + light.quadratic * (distance * distance));  

	ambient  *= attenuation; 
	diffuse  *= attenuation;
	specular *= attenuation;  
	
	float shadow = ShadowCalculation(light.position);
	vec3 lightning = ambient +  (1.0-shadow)*(diffuse + specular);
	return lightning;

}

void main()
{
	// preprocessing
	vec3 norm = normalize(aNormal);
	vec3 viewDir = normalize(cameraPos - FragPos);
	
	// Point light sources
	vec3 result = calculatePointLight(norm, viewDir);
	
	FragColor = vec4(result, 0.1);
}