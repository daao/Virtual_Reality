#version 330 core
layout (location=0) in vec3 position;
layout (location=1) in vec3 colors;

uniform mat4 M;
uniform mat4 C;
uniform mat4 Pr;

out vec4 myColor;

void main(){
	gl_Position = Pr*C*M*vec4(position.xyz, 1.0f);
	myColor = vec4(colors.xyz, 1.0f);
}