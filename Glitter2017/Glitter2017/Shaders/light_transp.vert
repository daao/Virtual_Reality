#version 330 core
layout (location=0) in vec3 position;
layout (location=1) in vec3 normal;
layout (location=2) in vec2 texCoords;

uniform mat4 M;
uniform mat4 C;
uniform mat4 Pr;

out vec3 myColor;
out vec3 FragPos;
out vec3 aNormal;
out vec2 texCoord;

void main(){
	gl_Position = Pr*C*M*vec4(position.xyz, 1.0f);
	myColor = position;
	FragPos = vec3(M * vec4(position.xyz, 1.0f));
	
	// remove the translation column, keep the rotation & scaling in the model matrix
	aNormal = vec3(M * vec4(normal.xyz, 0.0f)); 
	texCoord = texCoords;
}