#version 330 core
in vec3 texCoord;
out vec4 color;
uniform samplerCube cubeMap;

void main() {
  color = texture(cubeMap, texCoord);
}
