#version 330 core
layout (location = 0) in vec3 position;
layout (location = 3) in vec2 texCoords;

uniform mat4 M;
uniform mat4 C;
uniform mat4 Pr;

out vec3 texCoord;

void main() {
  gl_Position = Pr*C*M*vec4(position.xyz, 1.0f);
  texCoord = position;
}
